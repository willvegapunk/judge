#include <bits/stdc++.h>
using namespace std;

int main(){
    auto func = [](auto a, auto b){
        return a + b;
    };
    printf("%d\n", func(0b111'000, 300'200'100));
}
