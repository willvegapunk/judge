import json
from urllib.parse import quote

from config import gitlab


def real_url(filepath):
    print('Vega Test quote(filepath)', quote(filepath))
    return "http://localhost:8000/mission_vega/{}".format(
        quote(filepath))

data = [{
    'token': 'JudgeToken',
    'mission': json.dumps({
        'return': {
            'url': 'http://localhost:3019',
        },
        'prepare': {
            'file': {
                'main.cpp': real_url('submission/1/main.cpp'),
                'verdict.cpp': real_url('verdict/verdict.cpp')
            },
            'execute': [{
                'command': [
                    'g++',
                    '-std=c++11',
                    '-O2',
                    'main.cpp',
                    '-o',
                    'a.out',
                ],
                'memory_limit': 2097152,
                'stdout': 'stdout.txt',
                'stderr': 'stderr.txt',
            }, {
                'command': [
                    'g++',
                    '-std=c++11',
                    '-O2',
                    'verdict.cpp',
                    '-o',
                    'verdict',
                ],
                'memory_limit': 2097152,
                'stdout': 'stdout.txt',
                'stderr': 'stderr.txt',
            }],
        },
        'tasks': {
            'task': [{
                'file': {
                    '__TASK_ID__.in': real_url('testdata/1/1.in'),
                    '__TASK_ID__.out': real_url('testdata/1/1.out'),
                },
                'macro': {
                    '__TIME_LIMIT__': 1000,
                }
            }, {
                'file': {
                    '1.in': real_url('testdata/2/2.in'),
                    '1.out': real_url('testdata/2/2.out'),
                },
                'macro': {
                    '__TIME_LIMIT__': 500,
                }
            }],
            'execute': [{
                'command': [
                    './a.out',
                ],
                'stdin': '__TASK_ID__.in',
                'stdout': '__TASK_ID__.user_out',
                'scope': {
                    'enable': True,
                    'import': [
                        'a.out',
                        '__TASK_ID__.in',
                    ],
                    'export': [
                        '__TASK_ID__.user_out',
                        '__TASK_ID__.meta',
                    ],
                },
                'meta': '__TASK_ID__.meta',
                'record': {
                    'enable': True,
                    'stdout': 1024,
                    'stderr': 1024,
                },
                'time_limit': '__TIME_LIMIT__',

            }, {
                'command': [
                    './verdict', '__TASK_ID__.user_out', '__TASK_ID__.out'
                ],
                'stdout': 'verdict_result',
                'record': {
                    'enable': True,
                    'stdout': 1024,
                    'stderr': 1024,
                }
            }, {
                'command': [
                    'cat', '__TASK_ID__.meta',
                ],
            }, {
                'command': [
                    'cat', 'mission',
                ]
            }, {
                'command': [
                    'cat', 'mission_result',
                ]
            }, {
                'command': [
                    'cat', 'main.cpp',
                ]
            }]
        },
    })
}]
